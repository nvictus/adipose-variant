#!/usr/bin/env python
from __future__ import division

import numpy as np
import pandas
import gc

START = 52800000
STOP = 56000000
BINSIZE = 40000
THRESH = 0.1

# read in input variants
snps = pandas.read_csv(
    './pearson/input-variants.txt.gz',
    compression='gzip', 
    header=None, 
    names=['id', 'chrom', 'POS', 'POS2', 'genotype', 'occurrence'], 
    delimiter='\t')
num_snps = len(snps)

# the rows are snps sorted by bp position
# group them into genomic bins of equal width
interval_map = pandas.cut(snps['POS'], range(START, STOP+BINSIZE, BINSIZE))
gby = snps.groupby(interval_map)
num_bins = len(gby.groups)
snpcounts = [len(gby.get_group(interval)) for interval in sorted(gby.groups.keys())]

# find the row positions that mark the bin edges
edges = np.r_[0, np.cumsum(snpcounts)]
edge_pairs = zip(edges[:-1], edges[1:])

# Obtain an iterator to read in pairwise data in chunks.
# The pairwise data is (all snps) x (all snps) in row-major order
reader = pandas.read_csv(
    './pearson/variant-r2-filtered.txt.gz',
    compression='gzip',
    delimiter='\t',
    header=None,
    names=['snp1','snp2','r2'],
    iterator=True)

R2mean = np.zeros((num_bins, num_bins), dtype=float)
R2freq = np.zeros((num_bins, num_bins), dtype=float)
for i in xrange(num_bins):
    chunk = reader.get_chunk(snpcounts[i]*num_snps)
    gc.collect()
    for j, (j1, j2) in enumerate(edge_pairs):
        mu = 0.0
        freq = 0.0
        for k in xrange(num_bins):
            start = k*num_snps + j1
            stop = k*num_snps + j2
            x = chunk.iloc[start:stop,2]
            mu += x.sum()
            freq += np.sum(x >= THRESH)
        mu /= (snpcounts[i]*snpcounts[j])
        freq /= (snpcounts[i]*snpcounts[j])
        R2mean[i,j] = mu
        R2freq[i,j] = freq

np.savetxt('./pearson/pmatrix-binned-mean.txt.gz', R2mean)
np.savetxt('./pearson/pmatrix-binned-freq-t%s.txt.gz' % THRESH, R2freq)






