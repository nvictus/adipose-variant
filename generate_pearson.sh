
PATH=/broad/compbio/pouyak/local/bin:$PATH

# filter by chr:position range
grep-overlap -o -c2 0 /broad/compbio/pouyak/popgen/proj/1kg/phase1/data/EUR.txt.gz - <<< chr16 52800000 56000000 | gzip -c > input-variants.txt.gz

# compute pairwise r^2
grep-overlap -w 4000000 input-variants.txt.gz{,} | corr -m 1 -f pearson2 | gzip -c > variant-r2.txt.gz

# filter output to get just tab separated bp coords and r^2
zcat variant-r2.txt.gz | sed "s/|/\t/g" | cut -f 3,7,10 | gzip -c > variant-r2-filtered.txt.gz