We predict two variants within the association region to be causal for the genetic association with obesity:
	- rs1421085 in adipose tissue
	- rs11642015 in the brain


Look up HiC data for human fibroblasts:

rs1421085 (within predicted enhancer)	chr16: 53800954

Promoter IRX5	chr16:54964200-54965302  (red)
Promoter IRX3	chr16:54319016-54319016  (green)
Promoter FTO	chr16:53737375-53738194  (blue)
Promoter RPGRIP1L	chr16: 53737375-53738194
Promoter RBL2	chr16: 53467832-53468474
Promoter CHD9	chr16: 53088445-53089051
Promoter IRX6	chr16: 55357172-55357772
Promoter CRNDE	chr16: 54973696-54974296

color the 8 others like so: 
#E41A1C #377EB8 #4DAF4A #984EA3 #FF7F00 #FFFF33 #A65628 #F781BF