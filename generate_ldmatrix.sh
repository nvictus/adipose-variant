#reuse Tabix
#reuse Python-2.7
cd ldmatrix

PATH=../progs/plink-1.9:../progs/vcftools-0.1.12a/bin:$PATH
BASE_NAME=ALL-chr16-section
START=52800000
STOP=56000000

# URL=ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20110521/ALL.chr16.phase1_release_v3.20101123.snps_indels_svs.genotypes.vcf.gz
# echo "Fetch chr16 variants and filter by position"
# tabix -h $URL 16:$START:$STOP | gzip -c > "${BASE_NAME}.vcf.gz"
#
# It's faster to just use the data slicer at
# http://browser.1000genomes.org/Homo_sapiens/UserData/SelectSlice

echo "Converting vcf file to plink input formats (map and ped)"
# Note: Could apply some filtering here...
vcftools --gzvcf "../data/${BASE_NAME}.vcf.gz" --plink --out out
mv out.map "${BASE_NAME}.map"
mv out.ped "${BASE_NAME}.ped"
mv out.log "vcftools.log"

echo "Generating complete LD r2 map"
plink --file $BASE_NAME --r2 square0 bin single-prec --out out
mv out.ld.bin "${BASE_NAME}.ld.tril.bin"
mv out.log "plink.log"

echo "Bin and reduce the pairwise data"
python reduce_ldmatrix.py

echo "Done!"