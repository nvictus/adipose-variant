#!/usr/bin/env python
from __future__ import division

import numpy as np
import pandas

START = 52800000
STOP = 56000000
BINSIZE = 40000
THRESH = 0.1

# read in map file
snps = pandas.read_csv(
    './ldmatrix/ALL-chr16-section.map', 
    header=None, 
    names=['chrom', 'id', '_', 'POS'], 
    delimiter='\t')
total_snps = len(snps)

# the rows are snps sorted by bp position
# group them into genomic bins of equal width
interval_map = pandas.cut(snps['POS'], range(START, STOP+BINSIZE, BINSIZE))
gby = snps.groupby(interval_map)
num_bins = len(gby.groups)
snpcounts = [len(gby.get_group(interval)) for interval in sorted(gby.groups.keys())]

# find the row positions that mark the bin edges
edges = np.r_[0, np.cumsum(snpcounts)]
edge_pairs = zip(edges[:-1], edges[1:])

# load binary ld matrix
mmap = np.memmap('./ldmatrix/ALL-chr16-section.ld.tril.bin', dtype=np.float32)
mmap.resize(total_snps, total_snps)

# reduce the ld matrix  --> mean r2 values
R2mean = np.zeros((num_bins, num_bins), dtype=np.float32)
R2freq = np.zeros((num_bins, num_bins), dtype=np.float32)
for i, (i1, i2) in enumerate(edge_pairs):
    for j, (j1, j2) in enumerate(edge_pairs):
        x = mmap[i1:i2, j1:j2]
        R2mean[i,j] = np.mean(x[np.isfinite(x)])
        R2freq[i,j] = np.sum(x[np.isfinite(x)] >= THRESH)/snpcounts[i]/snpcounts[j]

np.savetxt('./ldmatrix/ldmatrix-binned-mean.txt.gz', R2mean)
np.savetxt('./ldmatrix/ldmatrix-binned-freq-t%s.txt.gz' % THRESH, R2freq)
